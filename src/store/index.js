import Vue from 'vue'
import Vuex from 'vuex';

const fb = require('@/firebaseConfig.js')

Vue.use(Vuex)

// handle page reload
// store.dispatch('fetchModels')
export const store = new Vuex.Store({
    state: {
        models: [],
        floor:[],
        floors:[],
        modelsperfloor: [],
        modelDetails: [],
        floorcount: 0,
    },
    actions: {
        fetchModels({ commit, state }) {
            var modelsArray = {};
            fb.ModelCollection.onSnapshot(querySnapshot => {
                querySnapshot.forEach(doc => {
                    var model = doc.data();
                    var key = doc.id;
                    modelsArray[key] = model;
                })
                store.commit('setModels', modelsArray)
            })
        },
        fetchFloors({ commit, state }) {
            var floorsArray = {};
            fb.FloorCollection.onSnapshot(querySnapshot => {
                querySnapshot.forEach(doc => {
                    var floor = doc.data();
                    var key = doc.id;
                    floorsArray[key] = floor;
                })
                store.commit('setFloors', floorsArray)
            })
        },
        fetchModelsPerFloor({ commit, state }, modelId) {
            fb.ModelCollection.doc(modelId).get().then(res => {
                var floor = res.data().floors;
                store.commit('setModelsPerFloor', floor)
            })
        },
        fetchModelDetails({ commit, state }, clientId) {
            fb.ModelCollection.doc(clientId).get().then(res => {
                commit('setModelDetails', res.data());
            }).catch(err => {
                console.log(err)
            })
        },
        fetchFloor({ commit, state }, floorId) {
            fb.FloorCollection.doc(floorId).onSnapshot(res => {
                var models = res.data().models;
                var i=0;
                Object.values(models).forEach(rest=>{
                    if(rest.available == false){
                        i++;
                    }
                })
                commit('setFloorCount', i)
                commit('setFloor', res.data());
            })
        },
        updateModel({commit, state}, model){
            var usersUpdate = {};
            usersUpdate[`models.${model.model}.available`] = model.available;
            usersUpdate[`models.${model.model}.name`] = model.name;
            usersUpdate[`models.${model.model}.sqft`] = model.sqft;
            fb.FloorCollection.doc(''+model.floor).update(usersUpdate);
        },
        createModel({commit, state}, model){
              /**
               * Reference: https://stackoverflow.com/questions/49915854/how-to-push-an-array-value-in-firebase-firestore
               */
            // var sfModelRef = fb.ModelCollection.doc(''+model.name);
            // return fb.db.runTransaction((t) => {
            //     return t.get(sfModelRef).then((doc) => {
            //         console.log(doc);
            //         if (!doc.exists) return;
            //         t.set(sfModelRef, { 
            //             floors:{ 
            //                 [1]:model.floor,
            //             } 
            //         }, 
            //         { merge: true });
            //     });
            // }).catch(console.log);

            var sfDocRef = fb.FloorCollection.doc(''+model.floor);
            return fb.db.runTransaction((t) => {
                return t.get(sfDocRef).then((doc) => {
                    if (!doc.exists) return;
                    t.set(sfDocRef, { 
                        models:{ 
                            [model.model]:{
                                name: model.name,
                                sqft: model.sqft,
                                available: model.available,
                            }
                        } 
                    }, 
                    { merge: true });
                });
            }).then(function() {
                console.log("Transaction successfully committed!");
                // var sfModelRef = fb.ModelCollection.doc(''+model.name);
                // return fb.db.runTransaction((t) => {
                //     return t.get(sfModelRef).then((doc) => {
                //         console.log(doc);
                //         if (!doc.exists) return;
                //         t.set(sfModelRef, { 
                //             floors:{ 
                //                 [1]:model.floor,
                //             } 
                //         }, 
                //         { merge: true });
                //     });
                // }).catch(console.log);
            }).catch(function(error) {
                console.log("Transaction failed: ", error);
            });
        }
    },
    mutations: {
        /** CLIENTS MUTATIONS */        
        setModels(state, val) {
            if (val) {
                Vue.set(state, 'models', val);
            } else {
                state.models = []
            }
        },
        setFloors(state, val) {
            if (val) {
                Vue.set(state, 'floors', val);
            } else {
                state.floors = []
            }
        },
        setModelsPerFloor(state, val) {
            if (val) {
                Vue.set(state, 'modelsperfloor', val);
            } else {
                state.modelsperfloor = []
            }
        },
        setModelDetails(state, val) {
            Vue.set(state, 'modelDetails', val);
        },
        setFloor(state, val) {
            if (val) {
                Vue.set(state, 'floor', val);
            } else {
                state.floor = []
            }
        },
        setFloorCount(state, val) {
            if (val) {
                Vue.set(state, 'floorcount', val);
            } else {
                state.floorcount = 0
            }
        },
        
    },
    getters:{
        getModels(state){
            return state.models
        },
        getModelDetails(state) {
            return state.modelDetails
        },
        getFloor(state){
            return state.floor
        },
        getModelsPerFloor(state){
            return state.modelsperfloor
        },
        getFloors(state){
            return state.floors
        },
        getFloorCount(state){
            return state.floorcount
        },
    }
})