// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import {store} from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueFilter from 'vue-filter';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import VuePlyr from 'vue-plyr'

// import Dragdealer from '../static/libs/dragdealer.js';
// Vue.use(Dragdealer, '$dragdealer');

Vue.config.productionTip = false
Vue.use(Vuetify)
Vue.use(VueFilter)
Vue.use(VuePlyr)
Vue.filter('floorFormat', function(num){
    var s=["th","st","nd","rd"],
    v=num%100;
    return num+(s[(v-20)%10]||s[v]||s[0]);
})
const testMixin = {
  mounted(){
    this.$nextTick(() => {
      setTimeout(() => {
        if(this.initDragDealer)
          this.initDragDealer();
        if(this.$refs.view)
          this.$refs.view.classList.add('active');
      }, 250);
    });
  }
}

Vue.mixin(testMixin);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created: () => {
      store.dispatch('fetchModels'),
      store.dispatch('fetchFloors')
  }
})
