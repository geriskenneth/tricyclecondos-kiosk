import { firebase } from "@firebase/app";
import "@firebase/firestore";

const config = {
  apiKey: "AIzaSyBKxGsLYg_yzQpP1t3taq2qd6SfxcqHrwA",
  authDomain: "tricycle-kiosk.firebaseapp.com",
  databaseURL: "https://tricycle-kiosk.firebaseio.com",
  projectId: "tricycle-kiosk",
  storageBucket: "tricycle-kiosk.appspot.com",
  messagingSenderId: "918674527110"
};
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()

// date issue fix according to firebase
const settings = {
    timestampsInSnapshots: true
}
db.settings(settings)

// // firebase collections
const ModelCollection = db.collection('models')
const FloorCollection = db.collection('floors')

export {
    db,
    ModelCollection,
    FloorCollection
}