import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/components/Landing'
import Building from '@/components/Building'
import Floor from '@/components/Floor/Floor'
import Model from '@/components/Models/Model'
import ModelOverview from '@/components/Models/Overview'
import BuilderStory from '@/components/BuilderStory'
import Neighbourhood from '@/components/Neighbourhood'
import Dashboard from '@/components/Dashboard'
import Interior from '@/components/Interior'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/building',
      name: 'Building',
      component: Building
    },
    {
      path: '/floor/:id',
      name: 'FloorID',
      component: Floor,
      props: true
    },
    {
      path: '/model',
      name: 'ModelOverview',
      component: ModelOverview
    },
    {
      path: '/model/:id',
      name: 'ModelID',
      component: Model,
      props: true
    },
    {
      path: '/builderstory',
      name: 'builderId',
      component: BuilderStory
    },
    {
      path: '/neighbourhood',
      name: 'Neighbourhood',
      component: Neighbourhood
    },
    {
      path: '/interior',
      name: 'Interior',
      component: Interior
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    }
  ]
})
